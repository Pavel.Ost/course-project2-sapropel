-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 10.0.0.105
-- Время создания: Июн 16 2021 г., 08:27
-- Версия сервера: 5.7.31-34
-- Версия PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `uskov_sapropel2021`
--

-- --------------------------------------------------------

--
-- Структура таблицы `dredger_template`
--

CREATE TABLE `dredger_template` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `img` varchar(50) NOT NULL,
  `pumping` varchar(50) NOT NULL,
  `power` varchar(50) NOT NULL,
  `mining_depth` varchar(50) NOT NULL,
  `length_of_haul` varchar(50) NOT NULL,
  `discription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dredger_template`
--

INSERT INTO `dredger_template` (`id`, `title`, `img`, `pumping`, `power`, `mining_depth`, `length_of_haul`, `discription`) VALUES
(1, 'Нижегородец-1', '/img/zem-nn-1.png', '400', '75', '8', '300', 'Земсняряд \"Нижегородец-1\" используется при расчистке русел рек, прудов, хозяйственных водоемов, каналов. Обеспечивает намыв песка - 30000 тонн в месяц. Вы можете купить земснаряда без технологических принадлежностей - 3 млн. руб.'),
(2, 'Нижегородец-2', '/img/zem-nn-2.png', '800', '250', '11', '900', 'Земсняряд \"Нижегородец-2\" используется в большинстве случаев при намыве строительных песков. Обеспечивает намыв песка до 80000 тонн в месяц. Цена земснаряда без технологических принадлежностей - 7 млн. руб.'),
(3, 'Нижегородец-3', '/img/zem-nn-3.png', '1600', '250', '16', '600', '\"Нижегородец-3\" может работать как от дизельно-генераторной установки мощностью 500 кВт, так и от транспортной подстанции. Обеспечивает намыв песка до 112000 тонн в месяц. Цена земснаряда без технологических принадлежностей - 13 млн. руб.');

-- --------------------------------------------------------

--
-- Структура таблицы `form`
--

CREATE TABLE `form` (
  `id` int(11) NOT NULL,
  `fullname` varchar(40) NOT NULL,
  `telephone` decimal(40,0) NOT NULL,
  `email` varchar(40) NOT NULL,
  `theme` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `form`
--

INSERT INTO `form` (`id`, `fullname`, `telephone`, `email`, `theme`, `message`, `when`) VALUES
(125, '1238a', '72831293893', 'pacela@awe.aiw', '', 'apowrle', '2021-03-23 17:56:38'),
(126, 'GP', '79283198328', 'pav@s.ru', '', 'palwe', '2021-03-23 17:57:20'),
(127, 'Павел', '78182126464', 'so@mail.ru', '', 'Dididi', '2021-04-02 14:28:41'),
(128, 'asd', '71342353245', 'aw@mai.ru', '', 'aker', '2021-04-04 10:28:19'),
(129, 'test', '79999999999', 'test@mail.ru', '', '128', '2021-04-04 10:34:56'),
(130, 'test', '70239102390', 'test@awe.ej', 'etst', 'test', '2021-04-04 19:16:26'),
(131, 'Усков Алексей Владимирович', '72782633344', 'uskovav@lad24.ru', 'кк', '', '2021-04-04 19:20:04'),
(132, 'vasya', '72312312312', 'ase@awe.ri', 'koawe,lawe', 'awe', '2021-04-04 19:24:32'),
(133, 'petya', '72312312312', 'ase@awe.ru', '', 'kmaed', '2021-04-04 19:24:52'),
(134, 'Люба', '76561564565', 'jfgud@jfg.tuh', '', 'mhmghm', '2021-04-04 19:25:02'),
(135, 'люба', '79119119119', 'fhgbdf@fjh.ru', '', 'Проверка связи', '2021-04-04 19:27:57'),
(136, 'фыв', '71231231233', 'apwk@raw.rk', '', 'kma,er', '2021-04-04 19:27:59'),
(137, 'Люба', '72020202020', 'jghbd@gh.tu', '', 'gfgjfgf', '2021-04-04 19:28:39'),
(138, 'Усков Алексей Владимирович', '79027826444', 'uskovav@lad24.r', '', '', '2021-04-04 19:58:15'),
(139, 'fdfhd', '75653546445', 'fhgffg@fhf.tu', '', 'hgkgkh', '2021-04-04 20:42:51'),
(140, 'mvm', '76526513235', 'bfjgb@fjh.tu', '', 'gffgfn', '2021-04-04 20:47:16'),
(141, 'Люба', '78565353435', 'fhgv@fhg.tu', '', 'bffbfdd', '2021-04-04 20:55:43'),
(142, 'fdtlkm', '72912412481', 'oave@r.ru', 'kaw', 'kmaw', '2021-04-04 20:56:53'),
(143, 'Люба ПОП', '73452353545', 'hgbyu@fdhdg.tu', '', 'gnggf', '2021-04-04 20:57:12'),
(147, 'awepalwe', '724', 'opaw@awr.errk', '', 'koawr', '2021-04-04 21:07:26'),
(148, 'awepalwe', '724', 'opaw@awr.errk', '', 'koawr', '2021-04-04 21:07:26'),
(152, 'aweawaw', '724124', 'awe@aw.ru', '', 'ikoawr', '2021-04-04 21:15:49'),
(153, 'aweawaw', '724124', 'awe@aw.ru', '', 'ikoawr', '2021-04-04 21:15:49'),
(154, 'aweawaw', '724124', 'awe@awre.ri', '', 'lpawe', '2021-04-04 21:23:07'),
(155, 'aweawaw', '724124', 'awe@awre.ri', '', 'lpawe', '2021-04-04 21:23:07'),
(156, 'awe', '724124', 'awe@aseawe.ru', 'awe', 'awe', '2021-04-04 21:24:05'),
(157, 'awe', '724124', 'awe@aseawe.ru', 'awe', 'awe', '2021-04-04 21:24:05'),
(159, 'tlphth', '7034899235', 'pwakr@awr.ark', 'oakw', 'olm,raw', '2021-04-04 21:24:48'),
(160, 'vasya', '72424', 'awe@awr.ri', 'koawraw', 'ol,aw', '2021-04-04 21:26:05'),
(161, 'vasya', '72424', 'awe@awr.ri', 'koawraw', 'ol,aw', '2021-04-04 21:26:05'),
(162, 'petya', '72355', 'awe@awe.ry', 'awr', 'awr', '2021-04-04 21:30:01'),
(163, 'petya', '72355', 'awe@awe.ry', 'awr', 'awr', '2021-04-04 21:30:01'),
(164, 'alll', '7232352', 'aw@awe.ry', 'awekr', 'awr', '2021-04-04 21:34:15'),
(165, 'alll', '7232352', 'aw@awe.ry', 'awekr', 'awr', '2021-04-04 21:34:15'),
(166, 'test', '722', 'awe@sd.rjj', 'akwe', 'olw,r', '2021-04-04 21:36:01'),
(167, 'test', '722', 'awe@sd.rjj', 'akwe', 'olw,r', '2021-04-04 21:36:02'),
(168, 'Павел', '70291249012', 'pavek@mai.ru', '12', 'фцщлу', '2021-04-11 18:12:27'),
(169, 'Павел', '72031239129', 'poavel@mau.ru', '', 'фцу', '2021-04-11 18:19:59'),
(170, 'пвел', '72938129348', 'pavel@mai.ru', '', 'koawe', '2021-04-11 18:21:29'),
(171, 'Павел', '72904819248', 'pavel@mai.ru', '', 'ok', '2021-04-11 18:22:35'),
(174, 'Усков Алексей Владимирович', '79027826334', 'uskovav@lad24.r', '', '123', '2021-04-18 13:45:04'),
(175, 'Усков Алексей Владимирович', '72782633344', 'uskov@iee.unn.ru', '', '123', '2021-04-18 13:47:58'),
(176, 'Усков Алексей Владимирович', '72782633344', 'uskov@iee.unn.ru', '', '44', '2021-04-18 13:49:51'),
(177, 'Усков Алексей Владимирович', '72782633344', 'uskovav@lad24.ru', '', '1213', '2021-04-18 13:52:32'),
(178, 'Усков Алексей Владимирович', '72782633355', 'superUser-1@mail.ru', '', '23', '2021-04-18 13:53:25'),
(179, 'Усков Алексей Владимирович', '72782633344', 'uskovav@lad24.ru', '', '313', '2021-04-18 13:56:46'),
(180, 'Михаил', '79035095191', 'hd-mng-ng@yahonty.ru', '', 'Добрый день! Хотел бы узнать ваши цены и условия по очистке озера 7 Га от водорослей в Московской области г. Ногинск', '2021-05-30 11:29:58'),
(181, 'Усков Алексей Владимирович', '72782633333', 'uskovav@lad24.ru', '', 'eee', '2021-05-31 13:17:42'),
(182, 'Александр', '79687111141', 'nag@transcom7.ru', 'Доставка Земснарядов', 'Транспортная компания 7 предлагает свои услуги по перевозке земнарядов и комплектующих как по всей России так и в страны Европы и СНГ(Киргизия, Казахстан, Узбекистан, Таджикистан, Туркмения). Возим  обычными тентами и негабаритными тралами.', '2021-06-01 11:32:48'),
(183, 'Усков Алексей Владимирович', '72782633344', 'uskovav@lad24.ru', '', '3232', '2021-06-04 08:44:46'),
(184, 'Елена', '79011988548', 'demi.lena2010@yandex.ru', '', 'Нужно почистить пруд около 100кв.м. Пруд сильно заросший  и\n\nзаболоченный, находится в Тульской области.', '2021-06-05 17:52:29'),
(185, 'сергей', '79690758888', 'svs09@mail.ru', '', 'очистка русла реки сетунь примерно 1 км', '2021-06-07 14:39:40');

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE `statuses` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `statuses`
--

INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
(1, 'администратор'),
(2, 'менеджер'),
(3, 'модератор');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `fullname`, `password`, `email`, `status`, `created_at`, `updated_at`) VALUES
(27, 'sapropelAdmin', 'админ', '$2y$10$2SKJn3IEvMpzjBNtEG.w4eoCa2SsQCHRdZbEPKRZc08IPSPNtArgy', 'sapropelAdmin@mail.ru', 1, '2021-03-21 09:03:30', '2021-03-21 20:03:30'),
(28, 'sapropelModerator', 'модератор', '$2y$10$7lkyaAypdHqj0JpG5rmBv.Qz4IU/j0p4uHoSQqYLyGsTk1sUVBbtu', 'sapropelModerator@mail.ru', 3, '2021-03-21 09:04:08', '2021-03-21 20:04:08'),
(29, 'sapropelManager', 'менеджер ', '$2y$10$9A.ZLKHT8mzU2B.eMcoEKu1jz0Yq5A64THwdQuIGOq4Sn1DFpzJ52', 'sapropelManager@mail.ru', 2, '2021-03-21 09:05:02', '2021-03-21 20:05:02');

-- --------------------------------------------------------

--
-- Структура таблицы `zem`
--

CREATE TABLE `zem` (
  `id` int(11) NOT NULL,
  `img` varchar(250) NOT NULL,
  `discription` varchar(250) NOT NULL,
  `price` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zem`
--

INSERT INTO `zem` (`id`, `img`, `discription`, `price`, `title`) VALUES
(1, '/img/zem-nn-1.png', 'Намыв песка до 30 000 тонн в месяц', 'До 3 млн. руб.', 'Нижегородец-1'),
(2, '/img/zem-nn-2.png', 'Намыв песка до 80 000 тонн в месяц', 'До 7 млн. руб.', 'Нижегородец-2'),
(3, '/img/zem-nn-3.png', 'Намыв песка до 112 000 тонн в месяц', 'До 13 млн. руб.', 'Нижегородец-3');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `dredger_template`
--
ALTER TABLE `dredger_template`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`status_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `zem`
--
ALTER TABLE `zem`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `dredger_template`
--
ALTER TABLE `dredger_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT для таблицы `statuses`
--
ALTER TABLE `statuses`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблицы `zem`
--
ALTER TABLE `zem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`status`) REFERENCES `statuses` (`status_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
