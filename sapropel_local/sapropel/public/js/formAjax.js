const formaterPhoneNumber = (value) => {
  return value
    .slice(3, 18)
    .replace(/\D/g, "")
    .replace(/(\d{3})(\d{3})(\d{2})(\d{2})/, "($1)-$2-$3-$4");
};

const forms = {
  main: $("#formAjax"),
  popup: $("popup-form__form"),
};

const br = "<br>";

$(document).ready(function () {
  let validInputs = {
    name: false,
    phone: false,
    email: false,
  };

  //Разблокировка сабмита через добавление класса
  const chechInputs = (data) => {
    if (Object.values(data).includes(false)) {
      $(".question__input-button").removeClass("active");
      $(".popup-form__input-button").removeClass("active");
    } else {
      $(".question__input-button").addClass("active");
      $(".popup-form__input-button").addClass("active");
    }
  };

  $("input[name='phone']").on("change input", function () {
    $(this).val(`+7 ${formaterPhoneNumber($(this).val())}`);
    if ($(this).val().length === 18) validInputs.phone = true;
    else validInputs.phone = false;
    chechInputs(validInputs);
  });

  $("input[name='name']").on("change input", function () {
    if ($(this).val() !== "") validInputs.name = true;
    else validInputs.name = false;
    chechInputs(validInputs);
  });

  $("input[name='email']").on("change input", function () {
    const validateEmailValue = (value) => {
      if (!value.includes("@")) return false;
      else {
        const mail = value.split("@")[1].split(".")[1];
        if (mail) return true;
        else return false;
      }
    };
    if (validateEmailValue($(this).val())) validInputs.email = true;
    else validInputs.email = false;
    chechInputs(validInputs);
  });

  $("#formAjax").submit(function (event) {
    event.preventDefault(); //перекрыли отправку формы

    const fullname = $("input[name='name']").val().trim();
    const telephone = $("input[name='phone']").val().trim().replace(/\D/g, "");
    const email = $("input[name='email']").val().trim();
    const message = $("textarea[name='message']").val().trim();
    const theme = $("input[name='theme']").val().trim();

    $("#formAjax").validate({
      //Правила
      rules: {
        name: { required: true, maxlength: 40 },
        phone: { required: true, minlength: 14 },
        email: { required: true, maxlength: 40, email: true },
      },
      //Тексты предупреждений
      messages: {
        name: {
          required: "<p class='error-text'>Обязательное поле!</p>",
          maxlength:
            "<p class='error-text'>Максимальное кол-во символов 40 единиц!</ap",
        },
        phone: {
          required: "<p class='error-text'>Обязательное поле!</p>",
          minlength: "<p class='error-text'>Некорректный телефон!</p>",
          number:
            "<p class='error-text'>Введите только цифры номера телефона!</p>",
        },
        email: {
          required: "<p class='error-text'>Обязательное поле!</p>",
          email: "<p class='error-text'>Введите адрес почты корректно!</p>",
          maxlength:
            "<p class='error-text'>Максимальное кол-во символов 40 единиц!</p>",
        },
      },
      //Обработчик и отправка данных
      submitHandler: function (form) {
        $(form).ajaxSubmit({
          target: ".question__result",
          url: "/ajax",
          type: "POST",
          cache: false,
          data: {
            fullname: fullname,
            telephone: telephone,
            email: email,
            theme: theme,
            message: message,
          },
          dataType: "html",

          beforeSend: function (data) {
            $("#formAjax").fadeOut();
            // preloader
            $("#preloader").css("display", "inline-block");
            $("#formAjax").prop("disable", true);
          },
          success: function (data) {
            $("#formAjax").trigger("reset");
            $("#formAjax").prop("disable", false);
            $(".question__result")
              .fadeIn()
              .html("Ваше сообщение отправлено!<br>Вам скоро перезвонят!");
            $("#preloader").css("display", "none");
            $(".question__result").css("display", "block");
          },
        });
      },
    });
  });

  // POPUP FORM
  $(".popup-form__form").submit(function (event) {
    event.preventDefault(); //перекрыли отправку формы

    const fullname = $("input[name='name']").val().trim();
    const telephone = $("input[name='phone']").val().trim().replace(/\D/g, "");
    const email = $("input[name='email']").val().trim();
    const message = $("textarea[name='message']").val().trim();

    $(".popup-form__form").validate({
      //Правила
      rules: {
        name: { required: true, maxlength: 40 },
        phone: { required: true, minlength: 14 },
        email: { required: true, maxlength: 40, email: true },
      },
      //Тексты предупреждений
      messages: {
        name: {
          required: "<p class='error-text'>Обязательное поле!</p>",
          maxlength:
            "<p class='error-text'>Максимальное кол-во символов 40 единиц!</ap",
        },
        phone: {
          required: "<p class='error-text'>Обязательное поле!</p>",
          minlength: "<p class='error-text'>Некорректный телефон!</p>",
          number:
            "<p class='error-text'>Введите только цифры номера телефона!</p>",
        },
        email: {
          required: "<p class='error-text'>Обязательное поле!</p>",
          email: "<p class='error-text'>Введите адрес почты корректно!</p>",
          maxlength:
            "<p class='error-text'>Максимальное кол-во символов 40 единиц!</p>",
        },
      },
      //Обработчик и отправка данных
      submitHandler: function (form) {
        $(form).ajaxSubmit({
          // target: ".question__result",
          url: "/ajax",
          type: "POST",
          cache: false,
          data: {
            fullname: fullname,
            telephone: telephone,
            email: email,
            theme: null,
            message: message,
          },
          dataType: "html",

          beforeSend: function (data) {
            $(".popup-form__form").fadeOut();
            // preloader
            $("#preloader").fadeIn();
          },
          success: function (data) {
            $("#preloader").fadeOut();
            $(".popup-form__success-message").fadeIn();
          },
        });
      },
    });
  });
  $("input[data-submit='main']").mouseup((e) =>
    $("#formAjax").trigger("submit")
  );
  $("input[data-submit='popup']").mouseup((e) =>
    $(".popup-form__form").trigger("submit")
  );
});
