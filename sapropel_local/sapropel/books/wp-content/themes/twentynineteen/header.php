<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	
	
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/normalize.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/libs/hamburgers.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/fonts.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/index.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/mixins.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/header.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/footer.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/styles.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/popup_form.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/not_ready.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/styles.css">
  <link rel="stylesheet" href="https://sapropel.info/public/style/css/.css">
  <link rel="icon" href="https://sapropel.info/public/favicon.ico" type="image/icon">
	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100&display=swap" rel="stylesheet">
<meta name="yandex-verification" content="a4679debff617c3d" />

		<?php wp_head(); ?>

</head>

<body oncontextmenu="return false" oncopy="return false;"  oncontextmenu="return false" onselectstart="return false;">	
	
  <div class="logo-container text-center">
    <a href="/"><img class="logo-container__image" src="https://sapropel.info/public/img/logo.svg" alt="logo" /></a>
    <p class="logo-container__subtitle">производственное и опытно-конструкторское предприятие</p>
  </div>

  <!-- Модификатор header_main-page добавляет баннеры в хедер и меняет меню -->
  <div class="header header_main-page">
    <button class="hamburger hamburger--spring js-menu-toggle header__hamburger" type="button">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>
	  
    <ul class="header__menu-nav flex-center">
      <li class="header__menu-link-wrapper">
        <a href="https://sapropel.info/about_us" class="header__menu-link text-uppercase" style='color: #fff'>О компании</a>
      </li>
      <li class="header__menu-link-wrapper">
        <a href="https://sapropel.info/rent_and_sale" class="header__menu-link text-uppercase" style='color: #fff'>Продажа земснарядов</a>
      </li>
      <li class="header__menu-link-wrapper">
        <a href="https://sapropel.info/types_of_jobs" class="header__menu-link text-uppercase" style='color: #fff'>Услуги</a>
      </li>
      <li class="header__menu-link-wrapper">
        <a href="https://sapropel.info/examples" class="header__menu-link text-uppercase" style='color: #fff'>Примеры работ</a>
      </li>
      <li class="header__menu-link-wrapper">
        <a href="https://sapropel.info/patents" class="header__menu-link text-uppercase" style='color: #fff'>Изобретения</a>
      </li>
      <li class="header__menu-link-wrapper">
        <a href="https://sapropel.info/reviews" class="header__menu-link text-uppercase" style='color: #fff'>Отзывы</a>
      </li>
      <li class="header__menu-link-wrapper">
        <a href="https://sapropel.info/contacts" class="header__menu-link text-uppercase" style='color: #fff'>Контакты</a>
      </li>
    </ul>

	</div>


