<footer class="bg-dark">
      <div class="footer wrapper">
		  
		  <!--
        
		  <div class="footer-content">
          <a href="/"><img class="footer-content__img" src="https://sapropel.info/public/img/logo.svg" alt="Logo"></a>

          <div class="footer-content__contacts">
            <div class="footer-content__contacts-item">
              <h2 class="footer-content__contacts-item__title">Нижний Новгород:</h2>
              <a class="footer-content__contacts-item__text" href="tel:+7 (905) 014-00-95">+7 (905) 014-00-95</a>
              <a class="footer-content__contacts-item__text display-inline" href="tel: 419-06-07"> 419-06-07,</a>
              <a class="footer-content__contacts-item__text display-inline" href="tel: 419-06-08"> 419-06-08</a>
            </div>

            <div class="footer-content__contacts-item">
              <h2 class="footer-content__contacts-item__title">Москва:</h2>
              <a class="footer-content__contacts-item__text" href="tel:+7 (910) 435-13-29">+7 (910) 435-13-29</a>
              <a class="footer-content__contacts-item__text" href="tel:+7 (095) 234-46-97">+7 (095) 234-46-97</a>
            </div>
          </div>
        </div>

-->


<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=70858720&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/70858720/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="70858720" data-lang="ru" /></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(70858720, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/70858720" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

      </div>

    </footer>


    <!-- JS -->
    <script src="https://sapropel.info/public/js/jquery-3.5.1.min.js"></script>
    <script src="https://sapropel.info/public/js/slick.min.js"></script>
    <script src="https://sapropel.info/public/js/main.js"></script>
    <script src="https://sapropel.info/public/js/formAjax.js"></script>
    <script src="https://sapropel.info/public/js/popup.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <!-- <script src="http://malsup.github.com/jquery.form.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>

<?php wp_footer(); ?>

</body>
</html>
