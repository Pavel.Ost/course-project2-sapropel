<?php 
namespace App\Controllers;

use App\Models\DataModel;


class Manager extends BaseController {

    public function index() {

        helper(['form']);

        $model = new DataModel();
        $data_form = $model->get_data('form');
        $data =[
            'title' => 'Данные с формы обратной связи',
            'data_form' => $data_form,
        ];
        if($this->request->getMethod() == 'get' && $this->request->getVar()) {
            $newData['id'] = $this->request->getVar('id');
            $data['data_form'] = $model->delete_data('form', $newData['id'], 'id');
        }
        echo view('admin/templates/header', $data);
		echo view('admin/pages/form_table', $data);
		echo view('admin/templates/footer');
    }
        
        
    
    public function delete() {

        if($this->request->getMethod() == 'get') {

            $model = new DataModel();
            $newData = [
                'id' => $this->request->getVar('id'),
            ];
            $model->delete_data('form', $newData['id'], 'id');

        }
        
        echo view('admin/templates/header', $data);
		echo view('admin/pages/form_table', $data);
		echo view('admin/templates/footer');
    }

}
