<div class="dredgers bg-light">
    <h2 class="dredgers__title content-title">Изготовление земснарядов</h2>
    <div class="dredgers__text-wrapper wrapper">
        <p class="dredgers__text content-text">
            Изготавливаем земснаряды более 30 лет (с 1988 года). У нас собственное производство по созданию
            земснарядов.
        </p>
        <p class="dredgers__text content-text">
            Наши земснаряды защищены 45-ю патентами на конструктивные разработки и технологию выполнения работ.
            У нас вы можете купить земснаряд разной мощности.<br><br>Наши земснаряды могут быть использованы в качестве плавучей насосной станции для подачи воды в жилищные поселки, для полива и орошения сельско-хозяйственной культуры согласно патенту №.
        </p>
    </div>

    <div class="dredgers__container">
        <h2 class="dredgers__title content-title">Купить земснаряд</h2>
        <div class="dredgers__cards flex-center">
            <?php foreach ($zem->getResult() as $row) : ?>
                <div class="dredgers__card flex-column">
                    <div class="dredgers__card-image-wrapper flex-center">
                        <img src="<?php echo base_url(); ?><?php echo $row->img; ?>" alt="dredger" class="dredgers__card-image">
                    </div>
                    <div class="dredgers__card-body">
                        <p class="dredgers__card-subtitle text-center"><?php echo $row->title; ?></p>
                        <p class="dredgers__card-text text-center"><?php echo $row->discription; ?></p>

                        <div class="dredgers__card-control flex-between">
                            <p class="dredgers__card-cost"><?php echo $row->price; ?></p>
                            <a class="dredgers__card-button-link" href="/dredger_template/<?php echo $row->id; ?>"><button class="dredgers__card-button btn-default">Подробнее</button></a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <h2 class="dredgers__title content-title">Патенты и свидетельства</h2>
    <div class="dredgers__text-wrapper wrapper">

        <p class="dredgers__text content-text">Созданные конструкции земснарядов защищены нашими патентами и
            авторскими свидетельствами на изобретения, имеют сертификаты соответствия.</p>
    </div>

    <div class="container">
        <img src="<?php echo base_url(); ?>/img/patents.png" alt="" style="max-width: 100%">
    </div>

    <div class="container" style="overflow: hidden">

        <!-- <h2 class="dredgers__title content-title">Характеристики земснарядов</h2>
        <table class="table table-striped my-5">
            <thead>
                <tr>
                <th scope="col">Название земснаряда</th>
                <th scope="col" class="text-center">Производительность</th>
                <th scope="col" class="text-center"> Цена (без технологических принадлежностей)</th>
                <th scope="col">Услуги, оказываемые с помощью наших земснарядов</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td>«Нижегородец ‒ 1»</td>
                <td class="py-2 text-center">40 000 т в месяц <br> или 25 000 м<sup>3</sup></td>
                <td class="text-center">4,5 млн. руб.</td>
                <td rowspan="4" >
                <ul>
                <li>- Очистка водоемов &#8212; от 120 руб. за м<sup>3</sup> вынутого грунта</li>
                <li>- Намыв песка &#8212; от 110 руб. за м3</li>
                <li>- Добыча удобрений (сапропеля) &#8212; от 110 руб. за м3</li>
                </ul>
                </td>
                </tr>
                <tr>
                <td>«Нижегородец ‒ 2»</td>
                <td class="py-2 text-center">72 000 – 80 000 т в месяц <br>  45 000 – 50 000 м<sup>3</sup></td>
                <td class="text-center">7,9 млн. руб.</td>
                </tr>
                <tr>
                <td>«Нижегородец ‒ 3»</td>
                <td class="py-2 text-center">96 000 – 112 000 т в месяц <br>  60 000 – 70 000 м<sup>3</sup></td>
                <td class="text-center"> 13 млн. руб.</td>
                </tr>
            </tbody>
        </table> -->

    </div>