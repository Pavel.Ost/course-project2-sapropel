<div class="reviews bg-light">
   <div class="reviews__wrapper wrapper">
      <h2 class="reviews__title content-title">Отзывы</h2>
      <div class="reviews__images">
         <img class="reviews__images-item" src="<?php echo base_url(); ?>/img/reviews_1.jpg" alt="Pic">
         <img class="reviews__images-item" src="<?php echo base_url(); ?>/img/reviews_2.jpg" alt="Pic">
         <img class="reviews__images-item" src="<?php echo base_url(); ?>/img/reviews_3.jpg" alt="Pic">
         <img class="reviews__images-item" src="<?php echo base_url(); ?>/img/reviews_4.jpg" alt="Pic">
         <img class="reviews__images-item" src="<?php echo base_url(); ?>/img/reviews_5.jpg" alt="Pic">
         <img class="reviews__images-item" src="<?php echo base_url(); ?>/img/reviews_6.jpg" alt="Pic">
         <img class="reviews__images-item" src="<?php echo base_url(); ?>/img/reviews_7.jpg" alt="Pic">
      </div>
   </div>
</div>