<div class="patents bg-dark">
   <div class="wrapper">
      <h2 class="patents__title content-title">Примеры работ</h2>

      <!-- Выкса -->

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Намыв территории под строительство толстолистового прокатного «Стана-5000» г. Выкса (Нижегородская область)</h3>
         <p class="patents-item__note">Объем намывных работ: более 6 млн м3. <br> Подготовка территории и строительство завода осуществлялась в период с 2007 по 2012 г.</p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Zemsnaryad-ZRS-M-v-karere-VMZ.jpg" alt="Pic">
               <p class="patent-item__description">Намыв проводился земснарядами ЗРС-М <br> Для намыва использовались от 3 до 5 земснарядов</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Rabota-zemsnaryadov-v-karere-Vyksunskogo-metallurgicheskogo-zavoda.jpg" alt="Pic">
               <p class="patent-item__description">Работа земснарядов в карьере <br> Выксунского металлургического завода</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Obshhij-vid-postroennogo-zavoda-Stan-5000-i-obustroennaya-prilegayushhaya-territoriya-sozdannaya-s-pomoshhyu-namyva-g.jpg" alt="Pic">
               <p class="patent-item__description">Общий вид завода «Стан-5000» <br> и обустроенная территория, <br> созданная с помощью намыва</p>
            </div>
         </div>
      </div>

      <!-- Очистка водоёмов -->

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Очистка водоёмов</h3>
         <p class="patents-item__note"></p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/ochistka-cheboksary.jpg" alt="Pic">
               <p class="patent-item__description">Очистка канала инженерной защиты Чебоксарского водохранилища</p>
            </div>
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/ochustka-galkino.jpg" alt="Pic">
               <p class="patent-item__description">Очистка заросшего водоёма в д. Галкино Нижегородской области</p>
            </div>
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/ochistka-svyatoe.jpg" alt="Pic">
               <p class="patent-item__description">Расчистка озера Святое <br> пос. Пушкино г. Дзержинск</p>
            </div>
         </div>
      </div>


      <!-- Добыча строительных песков -->

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Добыча строительных песков</h3>
         <p class="patents-item__note"></p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/dobycha-peskov-yadrin.jpg" alt="Pic">
               <p class="patent-item__description">Намыв и добыча песков для ООО «Сурстройсервис» <br> г. Ядрин Республика Чувашия</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/dobycha-peskov-chuvarley.jpg" alt="Pic">
               <p class="patent-item__description">Добыча песков для ООО «Карьер Чуварлейский» <br> д. Чуварлей Д.-Константиновского района</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/dobycha-peskov-vyksa.jpg" alt="Pic">
               <p class="patent-item__description">Добыча строительных песков <br> г. Выкса, Выксунский металлургический завод</p>
            </div>
         </div>
      </div>

      <!-- Намыв пляжей -->

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Намыв пляжей</h3>
         <p class="patents-item__note"></p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/namyv-plyazhey-vereya-1.jpg" alt="Pic">
               <p class="patent-item__description">Намыв пляжа в пос. Верхняя Верея Выксунского района <br> Заказчик: Благотворительный фонд "Система" г. Москва</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/namyv-plyazhey-dzerjinsk.jpg" alt="Pic">
               <p class="patent-item__description">Намыв пляжа в г. Дзержинск<br> Заказчик: Администрация г. Дзержинск</p>
            </div>

         </div>
      </div>

      <!-- Игумново -->

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Намыв грунта на территории полигона твердых бытовых отходов (пос. Игумново, Нижегородская область)</h3>
         <p class="patents-item__note">Санитарное состояние главной свалки в Нижнем Новгороде (пос. Игумново, Дзержинский район) являлось неудовлетворительным. Полигон постоянно тлеет, а в летнее время горит, и жители близлежащих поселков, населенных пунктов задыхаются от дыма и загрязнения воздуха.</p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Poligon-bytovyh-othodov.jpg" alt="Pic">
               <p class="patent-item__description">Полигон бытовых отходов, непроходимый для техники и людей. Показан процесс горения отходов</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Zemsnaryad-na-razrabotke-grunta.jpg" alt="Pic">
               <p class="patent-item__description">С помощью земснаряд «Нижегородец-1» водно-грунтовая смесь подавалась к выделенному месту очага возгорания</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Uchastok-poligona-posle-zamyva-peschanym-gruntom.jpg" alt="Pic">
               <p class="patent-item__description">После замыва участка полигона оставалась ровная экологически безопасная территория</p>
            </div>
         </div>
      </div>

      <!-- Восстановление русла реки Железница в Выксе -->

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Восстановление русла реки Железница в г. Выкса</h3>
         <p class="patents-item__note"></p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/vyksa-ruslo-narusheno.jpg" alt="Pic">
               <p class="patent-item__description">Нарушенное русло реки Железница в г. Выкса</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/vyksa-ruslo-vosstanovleno.jpg" alt="Pic">
               <p class="patent-item__description">Восстановленное русло реки Железница</p>
            </div>
         </div>
      </div>

      <!-- "Водный мир" Автозавод -->

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Намыв строительной площадки для строительства жилого дома в микрорайоне «Водный мир» Автозаводского района г. Н. Новгорода в 2007 году</h3>
         <p class="patents-item__note">Намыв производился в летний период, и строители приступили к возведению фундамента под жилой дом в течение одного месяца</p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Rabota-zemsnaryada-Nizhegorodets-1.jpg" alt="Pic">
               <p class="patent-item__description">Работа производилась с помощью земснаряда «Нижегородец–1»</p>
            </div>

            <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Namytaya-ploshhadka-dlya-stroitelstva.jpg" alt="Pic">
               <p class="patent-item__description">На рисунке показана намытая площадка, подготовленная для строительства</p>
            </div>
         </div>
      </div>

   </div>
</div>