<div class="patents bg-dark">
   <div class="wrapper">
      <h2 class="patents__title content-title">Изобретения</h2>
      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Машина для пожаротужения</h3>
         <p class="patents-item__note">(Авторское свидетельство на изобретение №1123593). Автор Согин А.В.</p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img1_1.jpg" alt="Pic">
               <p class="patent-item__description">1. Вход в водоем</p>
            </div>

            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img1_2.jpg" alt="Pic">
               <p class="patent-item__description">2. Забор воды и работа в водоеме</p>
            </div>

            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img1_3.jpg" alt="Pic">
               <p class="patent-item__description">3. Движение по местности</p>
            </div>
         </div>
      </div>

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Машина для ведения пудового рыбоводства</h3>
         <p class="patents-item__note">(Авторское свидетельство на изобретение №1123593). Автор Согин А.В.</p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img2_1.jpg" alt="Pic">
               <p class="patent-item__description">1. Вход в рыбоводный водоем</p>
            </div>

            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img2_2.jpg" alt="Pic">
               <p class="patent-item__description">2. Загрузка машины кормом</p>
            </div>

            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img2_3.jpg" alt="Pic">
               <p class="patent-item__description">3. Раздача корма рыбам</p>
            </div>
         </div>
      </div>

      <div class="patents__wrapper bg-light">
         <h3 class="patent-item__title">Самоходный земснаряд</h3>
         <p class="patents-item__note">(Патент №39900). Автор Согин А.В.</p>
         <div class="patent-item__wrapper">
            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img3_1.jpg" alt="Pic">
               <p class="patent-item__description">1. Вход в воду</p>
            </div>

            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img3_2.jpg" alt="Pic">
               <p class="patent-item__description">2. Работа в водоеме</p>
            </div>

            <div class="patent-item__img-container">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/patent-img3_3.jpg" alt="Pic">
               <p class="patent-item__description">3. Движение по воде</p>
            </div>
         </div>
      </div>
   </div>




</div>