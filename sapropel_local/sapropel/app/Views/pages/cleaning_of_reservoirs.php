<div class="water-cleansing water-cleansing_wrapper bg-dark">
  <div class="wrapper">
    <h1 class="water-cleansing__title content-title">Очистка и восстановление водоемов</h1>
    <h2 class="water-cleansing__subtitle">В услугу по очистке водоемов входит:</h2>
    <ul class="water-cleansing__list">
      <li class="water-cleansing__list-item content-text">Удаление ила, загнивающей растительности, донного мусора, топляка</li>
      <li class="water-cleansing__list-item content-text">Восстановление водной среды</li>
      <li class="water-cleansing__list-item content-text">Придание эстетической природной привлекательности водоёму и бизлежащей к нему территории</li>
    </ul>

    
    <h2 class="water-cleansing__subtitle">Этапы подготовительных работ, выполняемые нашими специалистами:</h2>

    <ul class="water-cleansing__list">
      <li class="water-cleansing__list-item content-text">Анализ характера донных отложений и профиля водного объекта</li>
      <li class="water-cleansing__list-item content-text">Промер глубины</li>
      <li class="water-cleansing__list-item content-text">Создание плана-карты водоёма для удобства работы оператора земснаряда</li>
      <li class="water-cleansing__list-item content-text">Подготовка проекта работ</li>
      <li class="water-cleansing__list-item content-text">Доставка необходимой техники и материалов до места заказчика</li>
      <li class="water-cleansing__list-item content-text">Создание на берегу иловой карты или площадки для размещения геотубов</li>
      <li class="water-cleansing__list-item content-text">Монтаж земснаряда и трубопровода до иловой карты</li>
    </ul>

    <div class="water-cleansing__buttons flex-around pb-5">
      <button class="water-cleansing__button water-cleansing__button_margin-top btn-default btn-default_big btn-popup">Оставить заявку</button>
    </div>    

      <!-- Очистка водоёмов -->
    
    <div class="patents__wrapper bg-light">
        <h2 class="water-cleansing__title content-title">Примеры работ по очистке и восстановлению водоемов</h2>
        <p class="patents-item__note"></p>
        <div class="patent-item__wrapper text-center">
          <div class="patent-item__img-container col-md-6 col-xl-4">
              <img class="patent-item__img" src="<?php echo base_url(); ?>/img/ochistka-cheboksary.jpg" alt="Pic">
              <p class="patent-item__description">Очистка канала инженерной защиты Чебоксарского водохранилища</p>
          </div>
          <div class="patent-item__img-container col-md-6 col-xl-4">
              <img class="patent-item__img" src="<?php echo base_url(); ?>/img/ochustka-galkino.jpg" alt="Pic">
              <p class="patent-item__description">Очистка заросшего водоёма в д. Галкино Нижегородской области</p>
          </div>
          <div class="patent-item__img-container col-md-6 col-xl-4">
              <img class="patent-item__img" src="<?php echo base_url(); ?>/img/ochistka-svyatoe.jpg" alt="Pic">
              <p class="patent-item__description">Расчистка озера Святое <br> пос. Пушкино г. Дзержинск</p>
          </div>

          <div class="patent-item__img-container col-md-6 col-xl-4">
              <img class="patent-item__img" src="<?php echo base_url(); ?>/img/vyksa-ruslo-narusheno.jpg" alt="Pic">
              <p class="patent-item__description">Нарушенное русло реки Железница в г. Выкса</p>
          </div>
          <div class="patent-item__img-container col-md-6 col-xl-4">
              <img class="patent-item__img" src="<?php echo base_url(); ?>/img/vyksa-ruslo-vosstanovleno.jpg" alt="Pic">
              <p class="patent-item__description">Восстановленное русло реки Железница</p>
          </div>    
          <div class="patent-item__img-container col-md-6 col-xl-4">
              <img class="patent-item__img" src="<?php echo base_url(); ?>/img/udalenye-vodorosli-nizh1.jpg" alt="Pic">
              <p class="patent-item__description">Удаление водорослей и углублении водоема земснарядом «Нижегородец-1»</p>
          </div>            
          <div class="patent-item__img-container col-md-6 col-xl-4">
              <img class="patent-item__img" src="<?php echo base_url(); ?>/img/laksha.jpg" alt="Pic">
              <p class="patent-item__description">Строительство хозяйственного водоема в пос. Лакша Богородского района</p>
          </div>    
          <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/Rabota-zemsnaryada-Nizhegorodets-1.jpg" alt="Pic">
               <p class="patent-item__description">«Водный мир» г. Нижний Новгород. Очистка городского водоема земснарядом «Нижегородец-1»</p>
          </div>
          <div class="patent-item__img-container col-md-6 col-xl-4">
               <img class="patent-item__img" src="<?php echo base_url(); ?>/img/raschistka-kanala.jpg" alt="Pic">
               <p class="patent-item__description">Расчистка заросшего канала</p>
          </div>
        </div>
    </div>

    <div class="water-cleansing__plates-wrapper flex-center">
      <div class="water-cleansing__plate flex-column bg-light">
        <div class="water-cleansing__plate-logo-wrapper">
          <img class="water-cleansing__plate-logo" src="<?php echo base_url(); ?>/img/water_drops.svg" alt="Pic">
        </div>
        <p class="water-cleansing__plate-text content-text">Работы по очистке водоёмов направлены на сохранение благоприятной водной среды, оборудование берегов, безопасных пляжей</p>
      </div>
      <div class="water-cleansing__plate flex-column bg-light">
        <div class="water-cleansing__plate-logo">
          <img src="<?php echo base_url(); ?>/img/temperature.svg" alt="Pic">
        </div>
        <p class="water-cleansing__plate-text content-text">Очистка водоёма снижает температуру воздуха вблизи водного объекта, повышает влажность, восстанавливает экосистему, что благоприятствует развитию водной флоры и фауны</p>
      </div>
      <div class="water-cleansing__plate flex-column bg-light">
        <div class="water-cleansing__plate-logo">
          <img src="<?php echo base_url(); ?>/img/wave.svg" alt="Pic">
        </div>
        <p class="water-cleansing__plate-text content-text">Очистку водоёма проводят не реже одного раза в пять лет. Санитарную очистку следует проводить 1 раз в год в конце весны</p>
      </div>
    </div>


    <p class="water-cleansing__text content-text">Очистка осуществляется быстро и с минимальными затратами. Земснаряд способен эффективно собирать и откачивать илистые отложения, песок и мусор со дна водоёма
       с помощью грунтового насоса. За сутки земснаряд поднимает со дна водоема от 500 до 3000 м3 грунта. Собранные загрязнения транспортируются с водой по системе трубопроводов на специальные баржи или иловые карты, 
       или укладываются в геотубы.</p>
    <p class="water-cleansing__cooperation content-title">Этапы сотрудничества</p>
    <div class="water-cleansing__cooperation-stages bg-light flex-around text-center">
      <div class="water-cleansing__cooperation-stage">
        <img src="<?php echo base_url(); ?>/img/draft.svg" alt="pic" class="water-cleansing__cooperation-stage-icon">
        <p class="water-cleansing__cooperaton-stage-text">Составляем проект</p>
      </div>

      <img class="water-cleansing__cooperation-arrow" src="<?php echo base_url(); ?>/img/arrow.svg" alt="pic">

      <div class="water-cleansing__cooperation-stage">
        <img src="<?php echo base_url(); ?>/img/contract.svg" alt="pic" class="water-cleansing__cooperation-stage-icon">
        <p class="water-cleansing__cooperaton-stage-text">Составляем договор</p>
      </div>

      <img class="water-cleansing__cooperation-arrow" src="<?php echo base_url(); ?>/img/arrow.svg" alt="pic">

      <div class="water-cleansing__cooperation-stage">
        <img src="<?php echo base_url(); ?>/img/works.svg" alt="pic" class="water-cleansing__cooperation-stage-icon">
        <p class="water-cleansing__cooperaton-stage-text">Выполняем работы</p>
      </div>
    </div>

    <p class="water-cleansing__about-prices content-text">Стоимость услуг по очистке водоёма – <b>от 120 руб. за 1 м3</b> вынутого грунта, и зависит от:</p>
    <ul class="water-cleansing__list">
      <li class="water-cleansing__list-item content-text">Разнообразия и сложности работ</li>
      <li class="water-cleansing__list-item content-text">Площади, глубины и состояния водоёма</li>
      <li class="water-cleansing__list-item content-text">Месторасположения водного объекта</li>
    </ul>

    <div class="water-cleansing__buttons flex-around">
      <button class="water-cleansing__button water-cleansing__button_margin-top btn-default btn-default_big btn-popup">Оставить заявку</button>
    </div>

    <p class="water-cleansing__subtitle">Наши основные заказчики:</p>

    <ul class="water-cleansing__list">
      <li class="water-cleansing__list-item content-text">Администрации городов и муниципалитетов</li>
      <li class="water-cleansing__list-item content-text">Загородные дачные объединения и посёлки коттеджного типа</li>
      <li class="water-cleansing__list-item content-text">Промышленные предприятия и агрофирмы</li>
      <li class="water-cleansing__list-item content-text">Загородные санатории и базы отдыха</li>
      <li class="water-cleansing__list-item content-text">Яхт-клубы</li>
      <li class="water-cleansing__list-item content-text">Рыбоводческие и охотничьи хозяйства</li>
    </ul>

    <div class="water-cleansing__jobs-button-wrapper flex-center">
      <a href="/types_of_jobs" class="water-cleansing__jobs-button-link"><button class="water-cleansing__jobs-button btn-default">К видам работ</button></a>
    </div>
  </div>
</div>